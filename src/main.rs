mod chip_8;
mod plugins;

use bevy::prelude::*;
use plugins::{Chip8Plugin, HelloPlugin};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(HelloPlugin)
        .add_plugin(Chip8Plugin)
        .run();
}
