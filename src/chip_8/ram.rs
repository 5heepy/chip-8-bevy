const RAM_SIZE_IN_BYTES: usize = 4096;

pub struct Ram {
    data: [u8; RAM_SIZE_IN_BYTES],
}

impl Ram {
    pub fn new() -> Ram {
        Ram { data: [0; 4096] }
    }

    pub fn write(&mut self, address: usize, data: &[u8]) -> usize {
        let number_of_bytes = data.len();

        if address + number_of_bytes >= RAM_SIZE_IN_BYTES {
            panic!(
                "Trying to write {} bytes at address {:#04X?} would exceed bounds({} bytes) of the RAM.",
                number_of_bytes, address, RAM_SIZE_IN_BYTES
            );
        }

        for (byte_index, byte) in data.iter().enumerate() {
            self.data[address + byte_index] = byte.clone();
        }

        number_of_bytes
    }

    pub fn read(&self, address: usize, bytes_to_read: usize) -> &[u8] {
        if address + bytes_to_read >= RAM_SIZE_IN_BYTES {
            panic!(
                "Trying to read {} bytes at address {:#04X?} would exceed bounds({} bytes) of the RAM.",
                bytes_to_read, address,
                RAM_SIZE_IN_BYTES
            );
        }

        &self.data[address..address + bytes_to_read]
    }
}
