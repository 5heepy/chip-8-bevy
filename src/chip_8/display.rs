const WIDTH: usize = 64;
const HEIGHT: usize = 32;

#[derive(Debug)]
pub struct Position {
    x: usize,
    y: usize,
}

impl Position {
    pub fn new(x: usize, y: usize) -> Position {
        Position { x, y }
    }
}

pub struct Display {
    pixels: [[bool; WIDTH]; HEIGHT],
}

impl Display {
    pub fn new() -> Display {
        Display {
            pixels: [[false; WIDTH]; HEIGHT],
        }
    }

    pub fn clear(&mut self) {
        self.pixels.fill([false; 64]);
    }

    pub fn draw_sprite(&mut self, sprite: &[u8], position: Position) -> bool {
        let mut collided = false;

        let mut row = position.y;

        let masks = [0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01];

        for line in sprite {
            if row >= HEIGHT {
                row = 0;
            }

            masks.iter().enumerate().for_each(|(i, mask)| {
                let mut column_index = position.x + i;

                if column_index >= WIDTH {
                    column_index -= WIDTH;
                }

                // Sprites are XORed onto the screen, if a pixel has been erased, then there was a collision
                let current_pixel = self.pixels[row][column_index];
                let new_pixel = (line & mask) != 0;
                let updated_pixel = current_pixel ^ new_pixel;

                if current_pixel && !updated_pixel {
                    collided = true;
                }

                self.pixels[row][column_index] = updated_pixel;
            });

            row += 1;
        }

        collided
    }
}

#[rustfmt::skip]
pub static FONT_DATA: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, /* "0" */
    0x20, 0x60, 0x20, 0x20, 0x70, /* "1" */
    0xF0, 0x10, 0xF0, 0x80, 0xF0, /* "2" */
    0xF0, 0x10, 0xF0, 0x10, 0xF0, /* "3" */
    0x90, 0x90, 0xF0, 0x10, 0x10, /* "4" */
    0xF0, 0x80, 0xF0, 0x10, 0xF0, /* "5" */
    0xF0, 0x80, 0xF0, 0x90, 0xF0, /* "6" */
    0xF0, 0x10, 0x20, 0x40, 0x40, /* "7" */
    0xF0, 0x90, 0xF0, 0x90, 0xF0, /* "8" */
    0xF0, 0x90, 0xF0, 0x10, 0xF0, /* "9" */
    0xF0, 0x90, 0xF0, 0x90, 0x90, /* "A" */
    0xE0, 0x90, 0xE0, 0x90, 0xE0, /* "B" */
    0xF0, 0x80, 0x80, 0x80, 0xF0, /* "C" */
    0xE0, 0x90, 0x90, 0x90, 0xE0, /* "D" */
    0xF0, 0x80, 0xF0, 0x80, 0xF0, /* "E" */
    0xF0, 0x80, 0xF0, 0x80, 0x80, /* "F" */
];
