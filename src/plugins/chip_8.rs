use crate::chip_8::Cpu as Chip8Cpu;

use bevy::prelude::{App, Commands, Plugin, Query};

pub struct Chip8Plugin;

impl Plugin for Chip8Plugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(add_cpu).add_system(tick_cpu);
    }
}

fn add_cpu(mut commands: Commands) {
    commands.spawn(Chip8Cpu::new());
}

fn tick_cpu(mut query: Query<&mut Chip8Cpu>) {
    for mut cpu in query.iter_mut() {
        println!("Ticking cpu");
        cpu.tick();
    }
}
